import React from 'react';
import './App.css';

function App() {
  return (
    <div className='list'>
      <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
        <li>Item 5</li>
      </ul>
    </div>
  );
}

export default App;
